package com.mel.zaragozamonumentos.data.entities

class Geometry(var type : String,
               var coordinates : List<Double>?)