package com.mel.zaragozamonumentos.data.source

import com.mel.zaragozamonumentos.data.entities.Monumento
import com.mel.zaragozamonumentos.data.entities.ResponseMonuments

interface IDataSource {
    suspend fun getMonuments():ResponseMonuments
}