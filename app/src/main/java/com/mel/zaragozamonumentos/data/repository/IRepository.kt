package com.mel.zaragozamonumentos.data.repository

import com.mel.zaragozamonumentos.data.entities.Monumento
import com.mel.zaragozamonumentos.data.entities.ResponseMonuments

interface IRepository {
    suspend fun getMonuments():ResponseMonuments?
}