package com.mel.zaragozamonumentos.data.entities

class ResponseMonuments (
    var type : String,
    var crs : Crs,
    var features : List<Features>
)