package com.mel.zaragozamonumentos.data.repository.impl

import com.mel.zaragozamonumentos.data.entities.Monumento
import com.mel.zaragozamonumentos.data.entities.ResponseMonuments
import com.mel.zaragozamonumentos.data.repository.IRepository
import com.mel.zaragozamonumentos.data.source.IDataSource
import com.mel.zaragozamonumentos.data.source.remote.RemoteDataSource
import retrofit2.Response
import javax.inject.Inject

class RepositoryImpl @Inject constructor(private val remoteDataSource: RemoteDataSource):IRepository {

    override suspend fun getMonuments(): ResponseMonuments? {
        return remoteDataSource.getMonuments()
    }
}