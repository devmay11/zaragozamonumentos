package com.mel.zaragozamonumentos.data.entities

class Monumento (
    var title : String,
    var horario : String,
    var image : String,
    var geometry : Geometry?

)