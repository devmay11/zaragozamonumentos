package com.mel.zaragozamonumentos.data.source.remote

import com.mel.zaragozamonumentos.api.INetworkApi
import com.mel.zaragozamonumentos.data.entities.Monumento
import com.mel.zaragozamonumentos.data.entities.ResponseMonuments
import com.mel.zaragozamonumentos.data.source.IDataSource
import javax.inject.Inject

class RemoteDataSource @Inject constructor(private val networkApi:INetworkApi):IDataSource {
    override suspend fun getMonuments(): ResponseMonuments {
        return networkApi.getMonuments()
    }
}