package com.mel.zaragozamonumentos.di

import com.mel.zaragozamonumentos.api.INetworkApi
import com.mel.zaragozamonumentos.common.MyConstants
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class NetworkModule {
    @Provides
    @Singleton
    fun providerRetrofit() : INetworkApi = Retrofit.Builder()
        .baseUrl(MyConstants.Network.API)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(INetworkApi::class.java)
}