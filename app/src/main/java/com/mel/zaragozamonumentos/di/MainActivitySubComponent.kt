package com.mel.zaragozamonumentos.di

import com.mel.zaragozamonumentos.ui.MapsActivity
import dagger.Subcomponent
import dagger.android.AndroidInjector

@Subcomponent
interface MainActivitySubComponent:AndroidInjector<MapsActivity> {
    @Subcomponent.Factory
    interface Factory: AndroidInjector.Factory<MapsActivity>
}