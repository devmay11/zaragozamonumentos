package com.mel.zaragozamonumentos.di

import android.app.Application
import com.mel.zaragozamonumentos.MyApp
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules =[AndroidInjectionModule::class,
    MainActivityModule::class,
    ViewModelModule::class,
    NetworkModule::class,
    GenericModule::class])
interface ApplicationComponent {
    fun inject(application:MyApp)
}