package com.mel.zaragozamonumentos.di

import com.mel.zaragozamonumentos.ui.MapsActivity
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjector
import dagger.multibindings.ClassKey
import dagger.multibindings.IntoMap

@Module(subcomponents = [MainActivitySubComponent::class])
interface MainActivityModule {
    @Binds
    @IntoMap
    @ClassKey(MapsActivity::class)
    fun bindMainActivityInjectorFactory(factory: MainActivitySubComponent.Factory): AndroidInjector.Factory<*>
}