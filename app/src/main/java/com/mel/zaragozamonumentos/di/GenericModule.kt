package com.mel.zaragozamonumentos.di

import android.provider.SyncStateContract
import com.mel.zaragozamonumentos.api.INetworkApi
import com.mel.zaragozamonumentos.common.MyConstants
import com.mel.zaragozamonumentos.data.repository.IRepository
import com.mel.zaragozamonumentos.data.repository.impl.RepositoryImpl
import com.mel.zaragozamonumentos.data.source.IDataSource
import com.mel.zaragozamonumentos.data.source.remote.RemoteDataSource
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
interface GenericModule {

    @Binds
    @Singleton
    fun bindRepository(repository: RepositoryImpl): IRepository

    @Binds
    fun bindRemoteDataSource(remoteDataSource: RemoteDataSource): IDataSource

}