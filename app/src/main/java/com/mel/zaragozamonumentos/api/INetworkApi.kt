package com.mel.zaragozamonumentos.api

import com.mel.zaragozamonumentos.data.entities.ResponseMonuments
import retrofit2.http.GET

interface INetworkApi {

    @GET("monumento.geojson?srsname=wgs84&rows=10&fl=title,horario,geometry,image")
    suspend fun getMonuments(): ResponseMonuments
}