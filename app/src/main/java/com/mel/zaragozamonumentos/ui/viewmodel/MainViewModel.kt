package com.mel.zaragozamonumentos.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mel.zaragozamonumentos.data.entities.Monumento
import com.mel.zaragozamonumentos.data.entities.ResponseMonuments
import com.mel.zaragozamonumentos.data.repository.IRepository
import com.mel.zaragozamonumentos.data.repository.impl.RepositoryImpl
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject

class MainViewModel @Inject constructor(val repositoryImpl: IRepository): ViewModel() {

    private val _listMonumentos = MutableLiveData<ResponseMonuments>()
    val listMonumentos: LiveData<ResponseMonuments> = _listMonumentos

    val eventError:SingleLiveEvent<Unit> = SingleLiveEvent()

    fun load(){
        viewModelScope.launch {
            try {
                repositoryImpl.getMonuments()?.run {
                    if (features.isEmpty()) eventError.call()
                    if (features.isNotEmpty()) _listMonumentos.value=this
                }?: run { eventError.call() }
            }catch (e:Exception){
                e.printStackTrace()
                eventError.call()
            }
        }
    }

}