package com.mel.zaragozamonumentos.ui

import android.content.DialogInterface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.maps.*

import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.maps.android.data.geojson.GeoJsonLayer
import com.mel.zaragozamonumentos.R
import com.mel.zaragozamonumentos.data.entities.Geometry
import com.mel.zaragozamonumentos.databinding.ActivityMapsBinding
import com.mel.zaragozamonumentos.di.ViewModelFactory
import com.mel.zaragozamonumentos.ui.viewmodel.MainViewModel
import dagger.android.AndroidInjection
import org.json.JSONObject
import javax.inject.Inject
import com.google.android.gms.maps.model.LatLngBounds




class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private lateinit var binding: ActivityMapsBinding
    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    val viewModel: MainViewModel by lazy {
        ViewModelProvider(this,viewModelFactory).get(MainViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidInjection.inject(this)
        binding = ActivityMapsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        viewModel.eventError.observe(this,{
            Toast.makeText(MapsActivity@this,R.string.error_loading_data, Toast.LENGTH_SHORT).show()
        })
        viewModel.load()
        binding.btn.setOnClickListener { viewModel.load() }
    }
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        viewModel.listMonumentos.observe(this,{
            val gson=Gson()
            val json=gson.toJson(it)
            val jsonObject=JSONObject(json)
            val geoJsonLayer=GeoJsonLayer(mMap,jsonObject)
            val latLngBoundsBuilder=LatLngBounds.Builder()
            geoJsonLayer.features.forEach { geoJsonFeature ->
                val geoJsonPoint=geoJsonFeature.geometry
                latLngBoundsBuilder.include(geoJsonPoint.geometryObject as LatLng)
            }
            latLngBoundsBuilder.build().run {
                mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(this, 10));
            }
            geoJsonLayer.setOnFeatureClickListener {
                AlertDialog.Builder(MapsActivity@this).run {
                    val horario=it.getProperty("horario")
                    setTitle(it.getProperty("title"))
                    setMessage(if(horario.isNullOrEmpty())"Sin horarios" else horario)
                    setNeutralButton("Aceptar"){dialog,which->
                        dialog.dismiss()
                    }
                    show()
                }
            }
            geoJsonLayer.addLayerToMap()
        })
    }
}